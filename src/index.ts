import { Scene, PerspectiveCamera, WebGLRenderer, BoxGeometry, MeshBasicMaterial, Mesh, PlaneGeometry, MeshStandardMaterial, MeshDepthMaterial, MeshPhysicalMaterial, AmbientLight, Light, AnimationLoader, AnimationMixer, Clock, DirectionalLight, DirectionalLightHelper } from "three";
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { CharacterControllerService } from './services/character-controller.service';
import "./global.scss";
import { UserInterfaceService } from "./services/user-interface.service";

/**
 * 开始初始化...
 */
const initialAnimation = async () => {
    /** 已暂停 */
    let isPaused = true;

    /** 
     * 初始化场景，镜头，控制器，渲染器
     */
    const initial = () => {
        const scene      = new Scene(); //场景
        const camera     = new PerspectiveCamera( 50, window.innerWidth / window.innerHeight ); //摄像头
        const renderer   = new WebGLRenderer(); // 渲染器
        const controller = new OrbitControls(camera, renderer.domElement); //控制器

        renderer.setSize( window.innerWidth, window.innerHeight );
        renderer.setClearColor("#a2c5e0");

        document.getElementById("main-canvas").appendChild( renderer.domElement );

        return {
            scene,
            controller,
            camera,
            renderer
        }
    }

    /**
     * 初始化光照
     */
    const initialSun = () => {
        const sun = new DirectionalLight();

        sun.shadow.camera.far    = 2000;
        sun.shadow.camera.left   = 1000;
        sun.shadow.camera.right  = -1000;
        sun.shadow.camera.top    = -1000;
        sun.shadow.camera.bottom = -1000;

        sun.position.set(100, 1000, 100);
        scene.add(sun);

        // const sunHelper = new DirectionalLightHelper(sun);
        // scene.add(sunHelper);

        const light = new AmbientLight();
        scene.add(light);
    }

    /**
     * 初始化角色
     */
    const initialCharacter = async () => {
        const loader         = new GLTFLoader();
        const character      = await loader.loadAsync('assets/idle.glb');
        const mixer          = new AnimationMixer(character.scene);
        
        const idleAction     = mixer.clipAction(character.animations[0]);

        const fbxLoader      = new FBXLoader();
        const run            = await fbxLoader.loadAsync('assets/run.fbx')
        const runAction      = mixer.clipAction(run.animations[0]);

        const walk           = await fbxLoader.loadAsync('assets/walk.fbx')
        const walkAction     = mixer.clipAction(walk.animations[0]);
        
        const fastRun        = await fbxLoader.loadAsync('assets/fast-run.fbx')
        const fastRunAction  = mixer.clipAction(fastRun.animations[0]);

        idleAction.play();

        const controller = new CharacterControllerService(
            character, 
            {
                idle: idleAction,
                run: runAction,
                walk: walkAction,
                fastRun: fastRunAction
            },
            {
                resume: () => {
                    userInterface.hide();
                    isPaused = false;
                    const interval = setInterval(() => {
                        controller.mouseZoomSpeed = -1;
                        controller.update(camera);
                    }, 10)
                    setTimeout(() => {
                        clearInterval(interval);
                    }, 300);
                },
                pause: () => {
                    userInterface.show();
                    const interval = setInterval(() => {
                        controller.mouseZoomSpeed = 1;
                        controller.update(camera);
                    }, 10)
                    setTimeout(() => {
                        clearInterval(interval);
                        isPaused = true;
                    }, 300);
                }
            }
        )

        character.scene.userData.animations = {
            mixer,
            controller
        }


        character.scene.traverse(c => {
            (c as any).material?.metalness && ((c as any).material.metalness = 0.5);
        })

        scene.add(character.scene);

        return character;
    }

    /**
     * 初始化地板
     */
    const initialFloor = () => {
        const floor = new PlaneGeometry(88, 88);
        const material = new MeshStandardMaterial( { color: "#8fc7a9" } );
        const planeFloor = new Mesh(floor, material);
        planeFloor.rotation.x = Math.PI / -2;   
        scene.add(planeFloor);     
    }

    /**
     * 执行初始化
     */
    const {scene, camera, renderer, controller} = initial();

    /** 初始化地图 */
    initialSun();
    initialFloor();

    /** 初始化界面 */
    const userInterface: UserInterfaceService = new UserInterfaceService()
    userInterface.show();

    /** 初始化角色 */
    const character = await initialCharacter()
    const clock     = new Clock();

    
    camera.position.z = 50;
    camera.position.y = 15;
    camera.position.x = 0;

    /**
     * 每一帧的处理
     */
    renderer.setAnimationLoop((time: number) => {
        /** 播放动画 */
        character.scene.userData.animations.mixer.update(clock.getDelta());

        /** 更新角色控制器 */
        character.scene.userData.animations.controller.update(camera);

        /** 更新全局控制器 */
        controller.target.set(
            character.scene.position.x,
            character.scene.position.y + 10,
            character.scene.position.z,
        );
        controller.update();

        /** 渲染 */
        !isPaused && renderer.render( scene, camera );
    });
    renderer.render( scene, camera );
}

/** 触发 */
initialAnimation();

