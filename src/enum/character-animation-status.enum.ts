export enum CharacterAnimationStatus {
    IDLE = 'idle',
    RUN = 'run',
    FAST_RUN = 'fastRun',
    TIRED_RUN = 'tiredRun',
    WALK = 'walk'
}