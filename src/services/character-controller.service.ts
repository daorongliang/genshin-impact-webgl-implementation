import { UserInterfaceService } from './user-interface.service';
import { AnimationAction, PerspectiveCamera, Vector3 } from 'three';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { CharacterAnimationStatus } from '../enum/character-animation-status.enum';

export class CharacterControllerService {

    /** 速度倍率 */
    public speedScale: number = 1;

    /** 快速跑模式 */
    public fastRunMode: boolean = false;
    public releasedFastRun: boolean = true;

    /** 移动速度 */
    public rightSpeed: number = 0; 
    public leftSpeed : number = 0; 
    public frontSpeed: number = 0; 
    public backSpeed : number = 0; 

    /** 禁止跑步 */
    public disableRun: boolean = false;

    /** 体力上限 */
    public maxPysicalStrength: number = 80;

    /** 剩余体力 */
    public pysicalStrength: number = 80;

    /** 扣除体力率 */
    public pysicalStrengthCost: number = 0.25;

    /** 允许回复体力 */
    public canRefillPhysicalStrength: boolean = false;

    /** 角色当前动画 */
    private currentAnimation: AnimationAction = this.animations.idle;

    /** 角色当前状态 */
    public currentStatus: CharacterAnimationStatus = CharacterAnimationStatus.IDLE;

    /** 鼠标锁定中心 */
    public mouseLocked: boolean = false;

    /** 鼠标放大速率 */
    public  mouseZoomSpeed: number;
    private mouseHorizontalSpeed: number;
    private mouseVerticalSpeed: number;


    public cameraPositionX: number = 0;
    public cameraPositionY: number = 30;
    public cameraPositionZ: number = 20;

    private characterLocalAngleRatio: number | typeof NaN = NaN;

    private previousGlobalAngleRatio: number = 0;

    private isPaused: boolean = false;

    /** 旋转循环函数 */
    private rotationInterval: NodeJS.Timer;

    constructor(
        private character: GLTF,
        private animations: {
            idle: AnimationAction,
            walk: AnimationAction,
            run: AnimationAction,
            fastRun: AnimationAction
        },
        private hooks: {
            pause: () => void,
            resume: () => void
        }
    ) {
        /** 键盘控制角色前后左右 */
        document.addEventListener('keydown', (evt: KeyboardEvent) => {
            if (!this.mouseLocked)
                return;

            switch (evt.key.toLowerCase()) {
                case 'd':
                    this.rightSpeed = 1;
                    break;
                case 's':
                    this.backSpeed  = 1;
                    break;
                case 'w':
                    this.frontSpeed = 1;
                    break;
                case 'a':
                    this.leftSpeed  = 1;
                    break;
                case 'shift':
                    /** 如果已经释放过快速跑 以及 并非行走模式 */
                    if (this.releasedFastRun && !this.disableRun) {
                        this.releasedFastRun = false; //释放冲刺改为false
                        this.fastRunMode = true; //冲刺模式改为true
                    }
                    break;            
                default:
                    break;
            }
            this.calculateCharacterLocalRotateAngle();
        })

        /** 键盘控制角色前后左右 */
        document.addEventListener('keyup', (evt: KeyboardEvent) => {
            if (evt.key == 'Escape')
                return this.togglePointerLock();
            if (!this.mouseLocked) return;

            switch (evt.key.toLowerCase()) {
                case 'd':
                    this.rightSpeed = 0
                    break;
                case 's':
                    this.backSpeed  = 0;
                    break;
                case 'w':
                    this.frontSpeed = 0;
                    break;
                case 'a':
                    this.leftSpeed  = 0;
                    break;
                case 'shift':
                    this.releasedFastRun = true;
                    this.fastRunMode = false;
                    document.getElementById("physical-strength-indicator").style.opacity = "0"; //隐藏体力条
                    !this.canRefillPhysicalStrength && this.delayPhysicalStrengthRefill(); //延迟回复体力
                    break;          
                case 'control':
                    this.toggleDisableRun(); //切换 奔跑 / 行走 模式
                default:
                    break;
            }
            this.calculateCharacterLocalRotateAngle();
        })

        /** 鼠标滑动 */
        let mouseStoppedInterval: NodeJS.Timeout;
        document.addEventListener("mousemove", (event: MouseEvent) => {
            if (!this.mouseLocked) return;
    
            this.mouseHorizontalSpeed = event.movementX * 0.005; // Mouse Movement Speed * Horizontal DPI
            this.mouseVerticalSpeed   = event.movementY / 8;
        
            if (mouseStoppedInterval)
                clearTimeout(mouseStoppedInterval)
        
            mouseStoppedInterval = setTimeout(() => {
                this.mouseHorizontalSpeed = 0;
                this.mouseVerticalSpeed   = 0;
            }, 100);
        })

        /** 监听锁定鼠标事件 */
        const changeCallback = () => {
            this.mouseLocked = !this.mouseLocked;
            if (this.mouseLocked) {
                // setTimeout(() => {
                //     this.isPaused = false;
                // }, 200);
                this.isPaused = false;
                this.hooks.resume();
            }
            else {
                setTimeout(() => {          
                    this.isPaused = true;
                }, 200);
                this.hooks.pause();
            }
        };
        document.addEventListener('pointerlockchange', changeCallback);
        document.addEventListener('mozpointerlockchange', changeCallback);
        document.addEventListener('webkitpointerlockchange', changeCallback);
    
        /** 监听鼠标滚轮 */
        let scrollStoppedInterval: NodeJS.Timeout;
        document.addEventListener("wheel", (event: WheelEvent) => {
            if (!this.mouseLocked) return;
            
            this.mouseZoomSpeed = event.deltaY / 8;
        
            if (scrollStoppedInterval)
                clearTimeout(scrollStoppedInterval)
        
            scrollStoppedInterval = setTimeout(() => {
                this.mouseZoomSpeed = 0;
            }, 100);
        });
    }

    /**
     * 每一帧更新处理
     * 
     * @param camera 摄像机
     */
    public update(camera: PerspectiveCamera) {
        if (this.isPaused) return;

        /** 快速跑扣除体力 */
        if (this.currentStatus == CharacterAnimationStatus.FAST_RUN) { // 如果已经再快速跑
            this.canRefillPhysicalStrength = false; // 设置无法回复体力
            this.pysicalStrength -= this.pysicalStrengthCost; // 扣除消耗体力
            this.updatePysicalStrengthUI(); // 更新体力条UI

            /** 如果体力值用完 */
            if (this.pysicalStrength <= 0) {
                /** 进入延迟回复状态 */
                this.delayPhysicalStrengthRefill();
            }            
        
        }
        else if (this.pysicalStrength != this.maxPysicalStrength) {//如果没有在奔跑 而且 体力值未回复满
            this.pysicalStrength += (this.canRefillPhysicalStrength ? this.pysicalStrengthCost : 0); // 回复体力值
            this.updatePysicalStrengthUI(); //更新体力条UI
        }
        else {
            document.getElementById("physical-strength-indicator").style.opacity = "0"; // 隐藏体力条UI
        }

        /** 如果体力值用完, 结束冲刺跑模式 */
        if (!this.pysicalStrength) {
            this.fastRunMode = false;
        }

        /** 冲刺模式下速度倍率为2 */
        this.speedScale = this.fastRunMode ? 2 : 1; 


        /** 设置移动坐标 */
        const horizontalSpeed = ( this.leftSpeed  - this.rightSpeed ) * this.speedScale;
        const verticalSpeed   = ( this.frontSpeed - this.backSpeed  ) * this.speedScale;
        const movement        = new Vector3(horizontalSpeed, 0, verticalSpeed);

        /** 移动角色 */
        // this.character.scene.position.add(movement);

        /** 移动摄像机 */
        camera.position.add(movement);

        /** 是否移动中 */
        const isMovment = horizontalSpeed || verticalSpeed;

        /** 如果正在移动, 禁止奔跑，但走路动画未播放 */
        if (isMovment && this.disableRun && this.currentStatus != CharacterAnimationStatus.WALK) {
            this.animationTransfer(CharacterAnimationStatus.WALK);
        }

        /** 如果角色静止，但发呆动画未播放 */
        if (!isMovment && this.currentStatus != CharacterAnimationStatus.IDLE) {
            this.animationTransfer(CharacterAnimationStatus.IDLE);
        }

        /** 如果角色正在移动，没有禁止奔跑，速率等于默认，并且未进入跑步动画 */
        if (isMovment && !this.disableRun && !this.fastRunMode && this.currentStatus != CharacterAnimationStatus.RUN) {
            this.animationTransfer(CharacterAnimationStatus.RUN);
        }

        /** 如果角色正在移动，没有禁止奔跑，速率大于默认，并且未进入快跑动画 */
        if (isMovment && !this.disableRun && this.fastRunMode && this.currentStatus != CharacterAnimationStatus.FAST_RUN) {
            this.animationTransfer(CharacterAnimationStatus.FAST_RUN);
        }

        /** 摄像头放大缩小 */
        if (
            this.cameraPositionZ - this.mouseZoomSpeed > 0
            && this.cameraPositionZ - this.mouseZoomSpeed < 30
        ) {
            this.cameraPositionZ -= this.mouseZoomSpeed;
        }


        /** 摄像头高度 */
        if (this.cameraPositionY + this.mouseVerticalSpeed > 0 
            && this.cameraPositionY + this.mouseVerticalSpeed < 50) {
            this.cameraPositionY += this.mouseVerticalSpeed;
        }
    
        /** 摄像头横移 */
        this.cameraPositionX = (this.cameraPositionX || 0) + (this.mouseHorizontalSpeed || 0);
        
        /** 计算角色转向角度 */
        const globalAngleRatio = (this.cameraPositionX % Math.PI) * 2;
        const globalAngle = 360 * (globalAngleRatio / 2 / Math.PI);
    
        /** 角色转向 */
        if (!isNaN(this.characterLocalAngleRatio)) {
            const targetAngel = globalAngleRatio / -1 - this.characterLocalAngleRatio;
            
            /** 设置转向循环，每5微秒转向 0.1/PI 度 */
            this.rotationInterval && clearInterval(this.rotationInterval);
            
            this.rotationInterval = setInterval(() => {
                if (Math.abs(Math.abs(targetAngel) - Math.abs(this.character.scene.rotation.y)) < 0.05) {
                    this.character.scene.rotation.y = targetAngel;
                    clearInterval(this.rotationInterval);
                }
                else if (this.character.scene.rotation.y < targetAngel && (targetAngel - this.character.scene.rotation.y) > Math.PI) {
                    if (this.character.scene.rotation.y > 0 && this.character.scene.rotation.y - 0.05 < 0) {
                        this.character.scene.rotation.y = this.character.scene.rotation.y - 0.05 + Math.PI * 2
                    }
                    else if (this.character.scene.rotation.y - 0.05 < Math.PI * -2) {
                        this.character.scene.rotation.y = this.character.scene.rotation.y - 0.05 + Math.PI * 2
                    }
                    else {
                        this.character.scene.rotation.y -= 0.05;
                    }
                }
                else if (this.character.scene.rotation.y < targetAngel) {
                    this.character.scene.rotation.y += 0.05;
                }
                else if (this.character.scene.rotation.y > targetAngel && (this.character.scene.rotation.y - targetAngel) > Math.PI) {
                    if (this.character.scene.rotation.y > 0 && this.character.scene.rotation.y + 0.05 > Math.PI * 2) {
                        this.character.scene.rotation.y = this.character.scene.rotation.y + 0.05 - Math.PI * 2;
                    }
                    else if (this.character.scene.rotation.y + 0.05 > 0) {
                        this.character.scene.rotation.y = this.character.scene.rotation.y + 0.05 - Math.PI * 2
                    }
                    else {
                        this.character.scene.rotation.y += 0.05;
                    }
                }
                else if (this.character.scene.rotation.y > targetAngel) {
                    this.character.scene.rotation.y -= 0.05;
                }
            }, 4);
        }
        this.previousGlobalAngleRatio = globalAngleRatio;


        /** Get Movement Distance */
        const objectPositionVerticalA = this.backSpeed - this.frontSpeed;
        const objectPositionVerticalB = Math.abs(Math.sin(globalAngleRatio) * objectPositionVerticalA) * Math.sign(objectPositionVerticalA);
        const objectPositionVerticalC = Math.abs(Math.sqrt(Math.pow(objectPositionVerticalA, 2) - Math.pow(objectPositionVerticalB, 2))) * Math.sign(objectPositionVerticalA);

        const objectPositionHorizontalA = this.rightSpeed - this.leftSpeed;
        const objectPositionHorizontalB = Math.abs(Math.sin(globalAngleRatio) * objectPositionHorizontalA) * Math.sign(objectPositionHorizontalA);
        const objectPositionHorizontalC = Math.abs(Math.sqrt(Math.pow(objectPositionHorizontalA, 2) - Math.pow(objectPositionHorizontalB, 2))) * Math.sign(objectPositionHorizontalA);

        /** Calculate Camera Position */
        const cameraDistanceA = 30 + this.cameraPositionZ;
        const cameraDistanceB = Math.abs(Math.sin(globalAngleRatio) * cameraDistanceA);
        const cameraDistanceC = Math.abs(Math.sqrt(Math.pow(cameraDistanceA, 2) - Math.pow(cameraDistanceB, 2)));

        let cameraPositionZ = 0,
            cameraPositionX = 0,
            objectPositionX = 0,
            objectPositionZ = 0;


        /** Adjust Distance */
        if (
            (0 <= globalAngle && globalAngle < 90) ||
            (-360 <= globalAngle && globalAngle < -270)
        ) {
            /** Top - Right */
            cameraPositionX = cameraDistanceB;
            cameraPositionZ = - cameraDistanceC;
    
            objectPositionX = objectPositionVerticalB - objectPositionHorizontalC;
            objectPositionZ = - objectPositionVerticalC - objectPositionHorizontalB;
        }
        else if (
            (90 <= globalAngle && globalAngle < 180) ||
            (-270 <= globalAngle && globalAngle < -180)
        ) {
            /** Bottom - Right */
            cameraPositionX = cameraDistanceB;
            cameraPositionZ = cameraDistanceC;
    
            objectPositionX = objectPositionVerticalB + objectPositionHorizontalC;
            objectPositionZ = objectPositionVerticalC - objectPositionHorizontalB;
        }
        else if (
            (180 <= globalAngle && globalAngle < 270) ||
            (-180 <= globalAngle && globalAngle < -90)
        ) {
            /** Bottom - Left */
            cameraPositionX = - cameraDistanceB;
            cameraPositionZ =   cameraDistanceC;
    
            objectPositionX = - objectPositionVerticalB + objectPositionHorizontalC;
            objectPositionZ = objectPositionVerticalC + objectPositionHorizontalB;
        }
        else if (
            (270 <= globalAngle && globalAngle < 360) ||
            (-90 <= globalAngle && globalAngle < 0)
        ) {
            /** Top - Left */
            cameraPositionX = - cameraDistanceB;
            cameraPositionZ = - cameraDistanceC;
    
            objectPositionX = - objectPositionVerticalB - objectPositionHorizontalC;
            objectPositionZ = - objectPositionVerticalC + objectPositionHorizontalB;
        }
    
        /** Update Camera Position */
        camera.position.x = this.character.scene.position.x + cameraPositionX;
        camera.position.y = this.character.scene.position.y + this.cameraPositionY;
        camera.position.z = this.character.scene.position.z + cameraPositionZ;

        this.character.scene.position.z += objectPositionZ;
        this.character.scene.position.x += objectPositionX;
    }

    /**
     * 更新体力值UI
     */
    private updatePysicalStrengthUI() {
        const a = -64.282; // 圆心常数
        const y = this.maxPysicalStrength - this.pysicalStrength + 5; // 剩余体力坐标 Y
        const x = (Math.sqrt(Math.pow(80, 2) - Math.pow(y - 45, 2)) + a).toFixed(5) // 剩余体力坐标 X
        document.getElementById("physical-strength-indicator-unused").setAttribute("d", `M 5,85 A 80 80 0 0 0 ${x} ${y}`); // 更新到HTML
        document.getElementById("physical-strength-indicator").style.opacity = "1"; // 显示体力UI
        if (this.pysicalStrength < 8) 
            document.getElementById("physical-strength-indicator-unused").style.stroke = "#ff3419"; // 显示体力UI为红色
        else
            document.getElementById("physical-strength-indicator-unused").style.stroke = "#f3d75a"; // 显示体力UI为黄色

    }

    /**
     * 动画转换
     */
    private animationTransfer(targetStatus: CharacterAnimationStatus) {
        /** 如果目标状态 等于 当前状态， 啥也不干 */
        if (targetStatus == this.currentStatus) return;

        let targetAnimation: AnimationAction;
        switch (targetStatus) {
            case CharacterAnimationStatus.WALK:
                targetAnimation = this.animations.walk
                break;
    
            case CharacterAnimationStatus.IDLE:
                targetAnimation = this.animations.idle
                break;
    
            case CharacterAnimationStatus.RUN:
                targetAnimation = this.animations.run
                break;
    
            case CharacterAnimationStatus.FAST_RUN:
                targetAnimation = this.animations.fastRun
                break;
                    
            default:
                break;
        }
        

        /** 播放行走动画 */
        targetAnimation.enabled = true;
        targetAnimation.play();

        /** 停止当前播放动画，过度到目标动画 */
        const animation = this.currentAnimation.crossFadeTo(targetAnimation, 0.2, true);

        /** 记录当前状态 */
        this.currentStatus = targetStatus;
        this.currentAnimation = animation;
    }
 
    /**
     * 体力延迟回复
     */
    public delayPhysicalStrengthRefill() {
        setTimeout(() => {
            this.canRefillPhysicalStrength = true;
        }, 1000);
    }

    /**
     * 切换行走或奔跑
     */
    public toggleDisableRun() {
        this.disableRun = !this.disableRun;

        const el = document.createElement("p");
        el.classList.add("hint");
        el.innerHTML = `已切换为${this.disableRun ? '行走' : '跑步'}状态`;
        document.getElementById('hints-container').appendChild(el);

        setTimeout(() => el.style.opacity = '1', 100);

        setTimeout(() => {
            el.style.opacity = '0';
            setTimeout(() => document.getElementById('hints-container').removeChild(el), 200);
        }, 1000);
    }
    

    /**
     * 进入/推出全面屏
     */
    togglePointerLock() {
        if (!this.mouseLocked) {
            document.documentElement.requestPointerLock();
        } else {
            document.exitPointerLock();
        }
    }

    /**
     * Calculate Character Local Rotate Angle
     */
    calculateCharacterLocalRotateAngle() {
        let horizontalAngle: number| null = null;
        let verticalAngle: number | null = null;
        let angleBasement: number = 0;

        /** Calculate Character Local Angle */
        const horizontalSpeed = this.rightSpeed - this.leftSpeed;
        const verticalSpeed = this.backSpeed - this.frontSpeed;
        if (horizontalSpeed) horizontalAngle = horizontalSpeed > 0 ? 90 : 270;
        if (verticalSpeed) verticalAngle = verticalSpeed > 0 ? 180 : (horizontalSpeed < 0 ?  360 : 0);
        

        if (horizontalAngle != null) angleBasement++;
        if (verticalAngle != null) angleBasement++;

        this.characterLocalAngleRatio = ((verticalAngle || 0) + (horizontalAngle || 0)) / angleBasement / 360 * Math.PI * 2;
    }
}