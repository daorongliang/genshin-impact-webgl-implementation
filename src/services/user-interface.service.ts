export class UserInterfaceService {

  private userInterface: HTMLDivElement;

  constructor() {
    this.userInterface = document.createElement("div");
    this.userInterface.classList.add("user-interface-content")
    this.userInterface.innerHTML = this.view;
  }

  /** 
   * 显示界面
   */
  show() {
    /** 模糊画面 */
    document.getElementById("user-interface").style.display = 'unset';
    document.getElementById("user-interface").appendChild(this.userInterface);
    setTimeout(() => {
      this.userInterface.style.opacity = '1';
      const rightMenu = document.querySelector(".menu-right") as HTMLDivElement;
      rightMenu.style.transform = 'translateX(0px)';
    }, 10);
  }
  
  /**
   * 隐藏界面
   */
  hide() {
    /** 取消模糊画面 */
    this.userInterface.style.opacity = '0';
    const rightMenu = document.querySelector(".menu-right") as HTMLDivElement;
    rightMenu.style.transform = 'translateX(-100px)';
    setTimeout(() => {
      document.getElementById("user-interface").style.display = 'none';
      document.getElementById("user-interface").removeChild(this.userInterface);
    }, 300);
  }

  /** 
   * 主界面
   */
  get view() {
    return `
      <div class="menu-left">
        <div class="btn btn-back">
          <i class="fa-solid fa-reply"></i>
        </div>
        <div class="btn btn-group">
          <div class="btn">
            <i class="fa-solid fa-camera"></i>
          </div>
          <div class="btn">
            <i class="fa-solid fa-chalkboard"></i>
          </div>
          <div class="btn">
            <i class="fa-solid fa-envelope"></i>
          </div>
          <div class="btn">
            <i class="fa-regular fa-clock"></i>
          </div>
          <div class="btn">
            <i class="fa-solid fa-gear"></i>
          </div>
        </div>
        <div class="btn btn-logout">
          <i class="fa-solid fa-arrow-right-from-bracket"></i>
        </div>
      </div>
      <div class="menu-right">
        <div class="menu-right-middle">
          <div class="menu-right-middle-left">
            <div class="avatar"></div>
            <div class="uid">UID 12345678</div>
            <div class="uid-copy">
              <i class="fa-solid fa-clone"></i>
              复制
            </div>
          </div>
          <div class="menu-right-middle-right">
            <div class="nickname bg-dark">
              <span>派蒙</span>
              <i class="ml-auto fa-solid fa-square-pen"></i>
            </div>
            <div class="signature">
              暂无签名
            </div>
            <div class="level bg-dark">
              <span>冒险等级</span>
            </div>
            <div class="exp">
              <div>
                <small>冒险阅历</small>
              </div>
              <div class="exp-indicator">
              </div>
            </div>
            <div class="world-level bg-dark">
              <span>世界等级</span>
            </div>
            <div class="dob">
              <span>生日</span>
            </div>
          </div>
        </div>
        <div class="menu-right-bottom">
          <div class="btn">
            <i class="fa-solid fa-ice-cream"></i>
            <p>队伍配置</p>
          </div>
          <div class="btn">
            <i class="fa-solid fa-bookmark"></i>
            <p>任务</p>
          </div>
          <div class="btn">
            <i class="fa-brands fa-gitlab"></i>
            <p>项目介绍</p>
          </div>
        </div>
      </div>
    `
  }
}